﻿using Meep.Tech.ModularData;
using Meep.Tech.ModularData.Mapping;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Meep.Tech.Games.Unity.Data.Mapper {

  /// <summary>
  /// Used to make archetype maps as monobehaviors that initialize on Awake
  /// </summary>
  /// <typeparam name="MappableType"></typeparam>
  /// <typeparam name="DataMapType"></typeparam>
  [DefaultExecutionOrder(-99)]
  public abstract class UnityArchetypeMapper<MappableType, DataMapType>
    : UnityArchetypeMapper
      , IArchetypeMapper<MappableType, DataMapType>
    where DataMapType : ArchetypeDataMap<MappableType>
    where MappableType : IMappable 
  {

    #region Constants

    #region Unity Set Values

    /// <summary>
    /// If this mapper should be enabled
    /// </summary>
    public bool IsEnabled;

    #endregion

    #region Abstract

    /// <summary>
    /// Name of the data folder, required
    /// </summary>
    protected abstract string MapedTypeDataFolderName {
      get;
    }

    /// <summary>
    /// None/Empty type's enumId
    /// </summary>
    protected virtual Enumeration NoneIdType
      => null;

    /// <summary>
    /// If we should construct the assembly classes
    /// </summary>
    public override bool ConstructAssemblyClasses
      => true;

    /// <summary>
    /// Passthrough
    /// </summary>
    public override string DataFolderForThisMappableType
      => mapper.DataFolderForThisMappableType;

    /// <summary>
    /// Passthrough
    /// </summary>
    public override Dictionary<string, Type> typesLeftToConstruct
      => mapper.typesLeftToConstruct;

    #endregion

    #endregion

    /// <summary>
    /// The internal mapper wrapper
    /// </summary>
    BasicArchetypeMapper<MappableType, DataMapType> mapper;

    #region Initialization

    /// <summary>
    /// Set up the mapper
    /// </summary>
    public override void initialize() {
      mapper = new BasicArchetypeMapper<MappableType, DataMapType>(
          MapedTypeDataFolderName,
          NoneIdType,
          ConstructAssemblyClasses
        );
      mapper.initialize();

      if (!IsEnabled) {
        mapper.disable();
      }
    }

    /// <summary>
    /// Initialize on awake.
    /// </summary>
    protected virtual void Awake() {
      initialize();
      startMappingThisArchetype();
    }

    /// <summary>
    /// Start mapping
    /// </summary>
    public override void startMappingThisArchetype()
      => mapper.startMappingThisArchetype();

    /// <summary>
    /// Set the mappable base type. only works before init
    /// </summary>
    public void setMappableBaseType(Type type)
      => mapper.setMappableBaseType(type);
    

    public override void tryToFinishMappingConstructableTypes()
      => mapper.tryToFinishMappingConstructableTypes();

    #endregion

    #region Data Access

    protected static bool TryToGetMappedData<MapperType>(string dataPropertyName, MappableType type, out object data, int? index = null) where MapperType : IArchetypeMapper<MappableType, DataMapType>
      => BasicArchetypeMapper<MappableType, DataMapType>.TryToGetMappedData<MapperType>(dataPropertyName, type, out data, index);

    protected static object GetMappedData<MapperType>(string dataPropertyName, MappableType type, int? index = null) where MapperType : IArchetypeMapper<MappableType, DataMapType>
      => BasicArchetypeMapper<MappableType, DataMapType>.GetMappedData<MapperType>(dataPropertyName, type, index);

    public bool tryToGetDataMapFor(MappableType type, out DataMapType dataMap)
      => mapper.tryToGetDataMapFor(type, out dataMap);

    public override IEnumerable<string> getValuesMappedFor(IMappable mappableType)
      => mapper.getValuesMappedFor(mappableType);

    #endregion
  }

  /// <summary>
  /// Base class for the generic
  /// </summary>
  public abstract class UnityArchetypeMapper : MonoBehaviour, IArchetypeMapper {
    public abstract bool ConstructAssemblyClasses { get; }
    public abstract string DataFolderForThisMappableType { get; }
    public abstract Dictionary<string, Type> typesLeftToConstruct { get; }

    public abstract IEnumerable<string> getValuesMappedFor(IMappable mappableType);
    public abstract void initialize();
    public abstract void startMappingThisArchetype();
    public abstract void tryToFinishMappingConstructableTypes();
  }
}
