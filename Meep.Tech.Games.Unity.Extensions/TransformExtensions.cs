﻿using System;
using UnityEngine;

public static class TransformExtensions {

  /// <summary>
  /// Get the first child recursively
  /// </summary>
  public static Transform FindRecursive(this Transform transform, string name) {
    if (transform.name == name) return transform;

    for (int i = 0; i < transform.childCount; i++) {
      Transform foundChild = transform.GetChild(i).FindRecursive(name);
      if (foundChild != null) return foundChild;
    }
    return null;
  }

  /// <summary>
  /// Do something on every recusive child
  /// </summary>
  public static void ForEachRecusiveChild(this Transform transform, Action<Transform> action) {
    for (int i = 0; i < transform.childCount; i++) {
      Transform child = transform.GetChild(i);
      action(child);
      child.ForEachRecusiveChild(action);
    }
  }
}
