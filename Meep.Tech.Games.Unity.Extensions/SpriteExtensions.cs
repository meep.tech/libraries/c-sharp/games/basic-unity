﻿using UnityEngine;

namespace Meep.Tech.Games.Unity.Extensions {

  /// <summary>
  /// General sprite extensions
  /// </summary>
  public static class SpriteExtensions {

    /// <summary>
    /// Load a file from an an immage in StreammedAssets
    /// </summary>
    /// <param name="assetPath"></param>
    /// <returns></returns>
    public static Sprite LoadFromFilePath(string assetPath) {
      //load basic texture
      Texture2D texture = Texture2DExtensions.LoadFromFilePath(assetPath);

      // Creates a new Sprite based on the Texture2D
      return texture.toSprite();
    }
  }

  /// <summary>
  /// Texture & sprite conversion
  /// </summary>
  public static class Texture2DExtensions {

    /// <summary>
    /// Load a file from an an immage in StreammedAssets
    /// </summary>
    /// <param name="assetPath"></param>
    /// <returns></returns>
    public static Texture2D LoadFromFilePath(string assetPath) {
      byte[] pngBytes = System.IO.File.ReadAllBytes(assetPath);

      //creates texture and loads byte array data to create image
      Texture2D texture = new Texture2D(2, 2);
      texture.LoadImage(pngBytes);

      // Creates a new Sprite based on the Texture2D
      return texture;
    }

    /// <summary>
    /// Make this into a sprite
    /// </summary>
    public static Sprite toSprite(this Texture2D texture) {
      return Sprite.Create(
        texture,
        new Rect(0.0f, 0.0f, texture.width, texture.height),
        new Vector2(0.5f, 0.5f),
        100.0f
      );
    }
  }
}
