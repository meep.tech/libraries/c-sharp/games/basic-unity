﻿using Newtonsoft.Json.UnityConverters.Helpers;
using System.Text;
using UnityEngine;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Reflection;

namespace UnityEngine {

  /// <summary>
  /// Extensions for vector3s in unity
  /// </summary>
  public static class VectorUtilities {

    /// <summary>
    /// replace the x value and return for chaining
    /// </summary>
    public static Vector3 ReplaceX(this Vector3 vector3, float x) {
      vector3.x = x;
      return vector3;
    }

    /// <summary>
    /// replace the y value and return for chaining
    /// </summary>
    public static Vector3 ReplaceY(this Vector3 vector3, float y) {
      vector3.y = y;
      return vector3;
    }
    /// <summary>
    /// replace the x value and return for chaining
    /// </summary>
    public static Vector2 ReplaceX(this Vector2 vector2, float x) {
      vector2.x = x;
      return vector2;
    }

    /// <summary>
    /// replace the y value and return for chaining
    /// </summary>
    public static Vector2 ReplaceY(this Vector2 vector2, float y) {
      vector2.y = y;
      return vector2;
    }

    /// <summary>
    /// replace the z value and return for chaining
    /// </summary>
    public static Vector3 ReplaceZ(this Vector3 vector3, float z) {
      vector3.z = z;
      return vector3;
    }

    /// <summary>
    /// Multiply two vector3s together
    /// </summary>
    /// <param name="vector3"></param>
    /// <param name="other"></param>
    /// <returns></returns>
    public static Vector3 Times(this Vector3 vector3, Vector3 other) {
      return new Vector3(
        vector3.x * other.x,
        vector3.y * other.y,
        vector3.z * other.z
      );
    }
  }
}

#region License
// The MIT License (MIT)
//
// Copyright (c) 2020 Wanzyee Studio
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#endregion

namespace Newtonsoft.Json.UnityConverters.Math {

  /// <summary>
  /// Custom Newtonsoft.Json converter <see cref="JsonConverter"/> for the Unity Vector3 type <see cref="Vector3"/>.
  /// </summary>
  public class Vector3Converter : PartialConverter<Vector3> {
    protected override void ReadValue(ref Vector3 value, string name, JsonReader reader, JsonSerializer serializer) {
      switch (name) {
        case nameof(value.x):
          value.x = reader.ReadAsFloat() ?? 0f;
          break;
        case nameof(value.y):
          value.y = reader.ReadAsFloat() ?? 0f;
          break;
        case nameof(value.z):
          value.z = reader.ReadAsFloat() ?? 0f;
          break;
      }
    }

    protected override void WriteJsonProperties(JsonWriter writer, Vector3 value, JsonSerializer serializer) {
      writer.WritePropertyName(nameof(value.x));
      writer.WriteValue(value.x);
      writer.WritePropertyName(nameof(value.y));
      writer.WriteValue(value.y);
      writer.WritePropertyName(nameof(value.z));
      writer.WriteValue(value.z);
    }
  }
}

namespace Newtonsoft.Json.UnityConverters.Helpers {
  internal static class JsonHelperExtensions {
    /// <summary>
    /// This refers to the ctor that lets you specify the line number and
    /// position that was introduced in Json.NET v12.0.1.
    /// <see cref="JsonSerializationException.JsonSerializationException(string, string, int, int, Exception)"/>
    /// <see href="https://github.com/JamesNK/Newtonsoft.Json/blob/12.0.1/Src/Newtonsoft.Json/JsonSerializationException.cs#L110"/>
    /// </summary>
    internal static readonly ConstructorInfo _JsonSerializationExceptionPositionalCtor
        = typeof(JsonSerializationException).GetConstructor(new[] {
                typeof(string), typeof(string), typeof(int), typeof(int), typeof(Exception)
        });

    private static JsonSerializationException NewJsonSerializationException(string message, string path, int lineNumber, int linePosition, [AllowNull] Exception innerException) {
      if (_JsonSerializationExceptionPositionalCtor != null) {
        return (JsonSerializationException)_JsonSerializationExceptionPositionalCtor.Invoke(new object[] {
                    message, path, lineNumber, linePosition, innerException
                });
      } else {
        return new JsonSerializationException(message, innerException);
      }
    }

    public static JsonSerializationException CreateSerializationException(this JsonReader reader, string message, [AllowNull] Exception innerException = null) {
      StringBuilder builder = CreateStringBuilderWithSpaceAfter(message);

      builder.AppendFormat(CultureInfo.InvariantCulture, "Path '{0}'", reader.Path);

      var lineInfo = reader as IJsonLineInfo;
      int lineNumber = default;
      int linePosition = default;

      if (lineInfo?.HasLineInfo() == true) {
        lineNumber = lineInfo.LineNumber;
        linePosition = lineInfo.LinePosition;
        builder.AppendFormat(CultureInfo.InvariantCulture, ", line {0}, position {1}", lineNumber, linePosition);
      }

      builder.Append('.');

      return NewJsonSerializationException(
          message: builder.ToString(), reader.Path, lineNumber, linePosition, innerException);
    }

    public static JsonWriterException CreateWriterException(this JsonWriter writer, string message, [AllowNull] Exception innerException = null) {
      StringBuilder builder = CreateStringBuilderWithSpaceAfter(message);

      builder.AppendFormat(CultureInfo.InvariantCulture, "Path '{0}'.", writer.Path);

      return new JsonWriterException(
          message: builder.ToString(), writer.Path, innerException);
    }

    private static StringBuilder CreateStringBuilderWithSpaceAfter(string message) {
      var builder = new StringBuilder(message);

      if (message.EndsWith(".")) {
        builder.Append(' ');
      } else if (!message.EndsWith(". ")) {
        builder.Append(". ");
      }

      return builder;
    }

    [return: MaybeNull]
    public static T ReadViaSerializer<T>(this JsonReader reader, JsonSerializer serializer) {
      reader.Read();
      return serializer.Deserialize<T>(reader);
    }

    public static float? ReadAsFloat(this JsonReader reader) {
      // https://github.com/jilleJr/Newtonsoft.Json-for-Unity.Converters/issues/46

      var str = reader.ReadAsString();

      if (string.IsNullOrEmpty(str)) {
        return null;
      } else if (float.TryParse(str, NumberStyles.Any, CultureInfo.InvariantCulture, out var valueParsed)) {
        return valueParsed;
      } else {
        return 0f;
      }
    }

    public static byte? ReadAsInt8(this JsonReader reader) {
      return checked((byte)(reader.ReadAsInt32() ?? 0));
    }
  }
}

#region License
// The MIT License (MIT)
//
// Copyright (c) 2020 Wanzyee Studio
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#endregion

namespace Newtonsoft.Json.UnityConverters {
  /// <summary>
  /// Custom base <c>Newtonsoft.Json.JsonConverter</c> to filter serialized properties.
  /// </summary>
  public abstract class PartialConverter<T> : JsonConverter
      where T : new() {
    protected abstract void ReadValue(ref T value, string name, JsonReader reader, JsonSerializer serializer);

    protected abstract void WriteJsonProperties(JsonWriter writer, T value, JsonSerializer serializer);

    /// <summary>
    /// Determine if the object type is <typeparamref name="T"/>
    /// </summary>
    /// <param name="objectType">Type of the object.</param>
    /// <returns><c>true</c> if this can convert the specified type; otherwise, <c>false</c>.</returns>
    public override bool CanConvert(Type objectType) {
      return objectType == typeof(T)
          || (objectType.IsGenericType
              && objectType.GetGenericTypeDefinition() == typeof(Nullable<>)
              && objectType.GenericTypeArguments[0] == typeof(T));
    }

    /// <summary>
    /// Read the specified properties to the object.
    /// </summary>
    /// <returns>The object value.</returns>
    /// <param name="reader">The <c>Newtonsoft.Json.JsonReader</c> to read from.</param>
    /// <param name="objectType">Type of the object.</param>
    /// <param name="existingValue">The existing value of object being read.</param>
    /// <param name="serializer">The calling serializer.</param>
    [return: MaybeNull]
    public override object ReadJson(
        JsonReader reader,
        Type objectType,
        [AllowNull] object existingValue,
        JsonSerializer serializer) {
      if (reader.TokenType == JsonToken.Null) {
        bool isNullableStruct = objectType.IsGenericType
            && objectType.GetGenericTypeDefinition() == typeof(Nullable<>);

        return isNullableStruct ? null : (object)default(T);
      }

      return InternalReadJson(reader, serializer, existingValue);
    }

    [return: MaybeNull]
    private T InternalReadJson(JsonReader reader, JsonSerializer serializer, [AllowNull] object existingValue) {
      if (reader.TokenType != JsonToken.StartObject) {
        throw reader.CreateSerializationException($"Failed to read type '{typeof(T).Name}'. Expected object start, got '{reader.TokenType}' <{reader.Value}>");
      }

      reader.Read();

      if (!(existingValue is T value)) {
        value = new T();
      }

      string previousName = null;

      while (reader.TokenType == JsonToken.PropertyName) {
        if (reader.Value is string name) {
          if (name == previousName) {
            throw reader.CreateSerializationException($"Failed to read type '{typeof(T).Name}'. Possible loop when reading property '{name}'");
          }

          previousName = name;
          ReadValue(ref value, name, reader, serializer);
        } else {
          reader.Skip();
        }

        reader.Read();
      }

      return value;
    }

    /// <summary>
    /// Write the specified properties of the object.
    /// </summary>
    /// <param name="writer">The <c>Newtonsoft.Json.JsonWriter</c> to write to.</param>
    /// <param name="value">The value.</param>
    /// <param name="serializer">The calling serializer.</param>
    public override void WriteJson(JsonWriter writer, [AllowNull] object value, JsonSerializer serializer) {
      if (value == null) {
        writer.WriteNull();
        return;
      }

      writer.WriteStartObject();

      var typed = (T)value;
      WriteJsonProperties(writer, typed, serializer);

      writer.WriteEndObject();
    }
  }
}